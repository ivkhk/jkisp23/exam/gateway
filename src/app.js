import { port } from "./config/index.js";
import express from "express";
import cors from "cors";
import router from "./routes/index.js";

const app = express();

app.use(cors());
app.disable("x-powered-by");

app.use(router);

app.listen(port, () => {
  console.log(`Gateway started at port ${port}`);
});
