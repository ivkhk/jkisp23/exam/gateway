import dotenv from "dotenv";
import { validatePort, validateIP } from "../utils/validation.js";

dotenv.config();

const port = process.env.GATEWAY_PORT;
validatePort(port,"GATEWAY_PORT");

const management = {};
management.ip = process.env.MANAGEMENT_IP;
management.port = process.env.MANAGEMENT_PORT;
validatePort(management.port,"MANAGEMENT_PORT");
validateIP(management.ip,"MANAGEMENT_IP");


const rabbitmq = {};
rabbitmq.ip = process.env.RABBITMQ_IP;
validateIP(rabbitmq.ip,"RABBITMQ_IP");

export  {port, management, rabbitmq};
