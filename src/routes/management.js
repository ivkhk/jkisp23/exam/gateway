import { createProxyMiddleware } from "http-proxy-middleware";
import { management } from "../config/index.js";

const managementProxy = createProxyMiddleware({
  target: `http://${management.ip}:${management.port}`,
  changeOrigin: true,
})

export { managementProxy };
