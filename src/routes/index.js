import express from "express";
import { managementProxy } from "./management.js";
import { rabbitmqHandler } from "../utils/rabbitmq.js";
import { handleJSONParsingError } from "../utils/errors.js";

const router = express.Router()

router.get("/", (req, res) => {
  res.statusCode = 200;
  res.send("Hello from Gateway!!!");
});

router.get("/api/users", managementProxy);
router.post('/api/users', express.json(), handleJSONParsingError, rabbitmqHandler)

export default router;
