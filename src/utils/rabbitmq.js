import amqp from "amqplib"
import { rabbitmq } from "../config/index.js";

export const rabbitmqHandler = async(req, res) => {

  const exchange = "amq.direct"
  const exchangeType = "direct"
  const routingKey = "management"

  const queue = "management_queue"

  try {
    const conn = await amqp.connect(`amqp://${rabbitmq.ip}`)
    const ch = await conn.createChannel()

    await ch.assertExchange(exchange,exchangeType, { durable: true })
    await ch.assertQueue(queue, "direct", { durable: true })
    await ch.bindQueue(queue, exchange, routingKey);

    ch.publish(exchange, routingKey, Buffer.from(JSON.stringify(req.body)))
    await ch.close()
    await conn.close()

  } catch (error) {
    console.error(`RabbitMQ Handler error: ${error}`)
  }
  return res.status(200).send("Server processing the request...")
}
