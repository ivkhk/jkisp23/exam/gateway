export const handleJSONParsingError = (err, req, res, next) => {
  try {
    JSON.parse(req.body)
  } catch (jsonError) {
    return res.status(400).json({ error: "Invalid JSON format!" })
  }
  next(err)
}
