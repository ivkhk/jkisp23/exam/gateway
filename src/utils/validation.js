export const validatePort = (port, envName) => {
  if (!port){
    console.error(`${envName} is undefined! You need to set environment variable!`);
    process.exit(1);
  }
  
  if (isNaN(port)){
    console.error(`${envName} should be a number!`);
    process.exit(1);
  }
  
  if (port < 1 || port > 65535){
    console.error(`${envName} should be in a valid range 1 - 65535!`);
    process.exit(1);
  }
}

export const validateIP = (ip, envName) => {
  if (!ip){
    console.error(`${envName} is undefined! You need to set environment variable!`);
    process.exit(1);
  }

 // if (net.isIP(ip) === 0){
 //   console.error(`${envName} is invalid!`);
 //   process.exit(1);
 // }
}
